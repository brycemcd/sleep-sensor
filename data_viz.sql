REFRESH MATERIALIZED VIEW parsed_sensor_readings;

WITH tm_series AS (
  SELECT
    tm::TIME as tm
    , tm::TIME + INTERVAL '10 seconds' as next_tm
  FROM generate_series('2019-12-01 00:00:00'::timestamp,
  '2019-12-02 23:59:59'::timestamp, '10 seconds') t(tm)
)
SELECT
COUNT(*) as cnt
, STDDEV(acc_x)
-- , STDDEV(acc_y)
-- , STDDEV(acc_z)
, AVG(temp)
, STDDEV(gyro_x)
, device_id
, ts.tm
FROM parsed_sensor_readings AS asr
  JOIN tm_series AS ts ON asr.insert_dttm::TIME BETWEEN ts.tm AND ts.next_tm
WHERE insert_dttm >= (NOW() - INTERVAL '20 minutes')
GROUP BY ts.tm, device_id
ORDER BY ts.tm, device_id
;
