DROP MATERIALIZED VIEW IF EXISTS parsed_sensor_readings;

CREATE MATERIALIZED VIEW parsed_sensor_readings AS
  SELECT
  SPLIT_PART(bed_device_reading, ',', 1)::INT as acc_x
  , SPLIT_PART(bed_device_reading, ',', 2)::INT as acc_y
  , SPLIT_PART(bed_device_reading, ',', 3)::INT as acc_z
  , SPLIT_PART(bed_device_reading, ',', 4)::NUMERIC as temp
  , SPLIT_PART(bed_device_reading, ',', 5)::INT as gyro_x
  , SPLIT_PART(bed_device_reading, ',', 6)::INT as gyro_7
  , SPLIT_PART(bed_device_reading, ',', 7)::INT as gyro_z
  , SPLIT_PART(bed_device_reading, ',', 8)::INT as device_id
  , insert_dttm
  FROM bed_vibration_readings
;
