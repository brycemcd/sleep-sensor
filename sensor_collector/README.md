# Bed Vibration Collector

Fitbit detects and reports sleep quality. I don't believe it's very
accurate. I put accelerometers under my mattress to see how
often/violent I move.

## Roadmap

+ [ ] Write data to database

## To Graph

```bash
mosquitto_sub -h mqtt01.thedevranch.net -t bedvibration |\
feedgnuplot --stream 0.2 \
  --points \
  --xlen 4000 \
  --lines \
  --legend 0 "sound intensity" \
  --title "sound intensity"
```

## Timeseries

```sql
WITH tm_series AS (
  SELECT
    tm::TIME as tm
    , tm::TIME + INTERVAL '15 seconds' as next_tm
  FROM generate_series('2019-01-01 00:00:00'::timestamp,
  '2019-01-01 23:59:59'::timestamp, '15 seconds') t(tm)
)
SELECT
COUNT(*) as cnt
, STDDEV(sound_intensity_reading)
, VARIANCE(sound_intensity_reading)
FROM ambient_sound_readings AS asr
  JOIN tm_series AS ts ON asr.insert_dttm::TIME BETWEEN ts.tm AND ts.next_tm
WHERE insert_dttm >= (NOW() - INTERVAL '30 minutes')
GROUP BY ts.tm
;
```
