package main

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"time"

	"database/sql"
	_ "github.com/lib/pq"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

const (
	MQTT_HOST   = "mqtt://mqtt01.thedevranch.net:1883/bedvibration"
	PSQL_HOST        = "psql02.thedevranch.net"
	PSQL_PORT        = 5432
	PSQL_DBNAME      = "bed_vibration"
	ISO8601_FMT = "2006-01-02T15:04:05.0000+00:00"
)

var (
	PSQL_USER = os.Getenv("PSQL_USER")
	PSQL_PASSWORD = os.Getenv("PSQL_PASSWORD")
  MQTT_CLIENT_NAME = os.Getenv("MQTT_CLIENT_NAME")
)

type MQTTMessage struct {
	SensorValue  string
	CapturedDttm string
}

func connect(client mqtt.Client) mqtt.Client {
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
	}

	if err := token.Error(); err != nil {
		log.Fatal(err)
	}

	return client
}

func createClient(clientId string, uri *url.URL) mqtt.Client {
	opts := createClientOptions(clientId, uri)
	client := mqtt.NewClient(opts)

	return client
}

func createClientOptions(clientId string, uri *url.URL) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s", uri.Host))
	opts.SetResumeSubs(true)
	opts.SetUsername(uri.User.Username())
	password, _ := uri.User.Password()
	opts.SetPassword(password)
	opts.SetClientID(clientId)

	opts.SetAutoReconnect(true)
	opts.SetKeepAlive(2)
	//opts.SetPingTimeout(2 * time.Second)
	//opts.SetResumeSubs(true)
	opts.SetClientID(MQTT_CLIENT_NAME)

	opts.SetConnectionLostHandler(func(client mqtt.Client, err error) {
		fmt.Println("CONNECTION LOST")
		fmt.Println("err: ", err)
	})

	return opts
}

func createDBConnection() (*sql.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		PSQL_HOST, PSQL_PORT, PSQL_USER, PSQL_PASSWORD, PSQL_DBNAME)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Println("Error opening pg", err)
		os.Exit(1)
	}

	// make sure we can actually connect to the thing
	err = db.Ping()
	if err != nil {
		panic(err)
	}

	return db, err
}

func mqttListen(client mqtt.Client, topic string, sendChan chan<- MQTTMessage) {
	client.Subscribe(topic, 0, func(client mqtt.Client, msg mqtt.Message) {

		mqttMessage := MQTTMessage{
			SensorValue:  string(msg.Payload()),
			CapturedDttm: time.Now().Format(ISO8601_FMT),
		}

		sendChan <- mqttMessage
	})
}

func writeChannelMessagesToDB(rcvChan <-chan MQTTMessage, db *sql.DB) {
	sqlStatement := `
  INSERT INTO bed_vibration_readings (bed_device_reading, insert_dttm)
  VALUES ($1, $2)`

	for msg := range rcvChan {
		//fmt.Println("line: ", msg)
		_, err := db.Exec(sqlStatement, msg.SensorValue, msg.CapturedDttm)
		if err != nil {
			fmt.Println("error: ", err)
			panic(err)
		}
	}
}

func main() {
  REQD_CONSTANTS := [...]string{
    "PSQL_USER",
    "PSQL_PASSWORD"}

  for _, key := range REQD_CONSTANTS {
    if(os.Getenv(key) == "") {
      log.Fatal("Required ENV VAR not present: ", key)
    }
  }

  msgChannel := make(chan MQTTMessage)

	fmt.Println("firing up: ", MQTT_HOST)
	uri, err := url.Parse(MQTT_HOST)
	if err != nil {
		log.Fatal(err)
	}
	topic := uri.Path[1:len(uri.Path)]
	if topic == "" {
		topic = "test"
	}

	db, err := createDBConnection()
	if err != nil {
		fmt.Println("error: ", err)
		panic(err)
	}
	defer db.Close()

	client := createClient(MQTT_CLIENT_NAME, uri)
	go mqttListen(client, topic, msgChannel)
	go writeChannelMessagesToDB(msgChannel, db)

	// loop forever so the subscriber does its thing
	for {
		if !client.IsConnected() {
			fmt.Println("CLIENT IS NOT CONNECTED")
			connect(client)
			mqttListen(client, topic, msgChannel)
		}
	}
}
