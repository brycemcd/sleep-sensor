-- CREATE DATABASE bed_vibration;

DROP TABLE IF EXISTS bed_vibration_readings;

CREATE TABLE bed_vibration_readings (
  id SERIAL NOT NULL PRIMARY KEY
  , bed_device_reading VARCHAR(200) NOT NULL DEFAULT 'ERROR'
  , insert_dttm TIMESTAMP NOT NULL DEFAULT NOW()
);
